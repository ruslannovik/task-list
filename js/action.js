
// поля, свойства (ключ: значение)



let tasks = [];
getTasks();

// -------------- false way ----------------------

/*
function drawTask(task) {
    return `<div class="task">
                <input type="checkbox" class="task__complete" ${ task.isComplete ? 'checked' : '' }>
                <span class="task__number">${task.id}</span>
                <span class="task__name">${task.name}</span>
            </div>`;
}

const list = document.querySelector('.container__list');
tasks.forEach(item => {
    list.innerHTML += drawTask(item);
});

*/

// ---------------------- true way---------------

function createTaskTag(task) {
    const name = document.createElement('span');  // name = <span></span>
    name.className = 'task__name task__name_active'; // name = <span class = "task__name"></span>
    name.innerText = task.name; // name = <span class = "task__name"> Выгулять собаку</span>

    const number = document.createElement('span');
    number.className = 'task__number';
    number.innerText = task.id + 1;

    const checkbox = document.createElement('input');
    checkbox.type = 'checkbox';
    checkbox.className = 'task__complete';
    checkbox.checked = task.isComplete;

    const deleteBtn = document.createElement('div');
    deleteBtn.className = 'task__delete';
    deleteBtn.innerText = 'x'; 
    deleteBtn.id = 'task_' + task.id; // <div class="task__delete" id="task_1">x</div>
    deleteBtn.addEventListener('click', itemClick);

    const editBtn = document.createElement('div');
    editBtn.className = 'edit';
    editBtn.innerText = 'e';
    editBtn.id = 'task_' + task.id;
    editBtn.addEventListener('click', toggleEdit)

    const editInput = document.createElement('input');
    editInput.className = 'task__edit';
    editInput.value = task.name;

    const taskTag = document.createElement('div');
    taskTag.className = 'task'; //taskTag = <div class = "task"></div>
    taskTag.appendChild(checkbox);// taskTag = <div class = "task"><input...></task>
    taskTag.appendChild(number);// taskTag = <div class = "task"><input...><span...></task>
    taskTag.appendChild(name);// taskTag = <div class = "task"><input...><span...><span...></task>
    taskTag.appendChild(editInput);
    taskTag.appendChild(editBtn);
    taskTag.appendChild(deleteBtn);

    return taskTag;
}

const list = document.querySelector('.container__list');

drawTask();

function drawTask() {
    list.innerHTML = '';
    tasks.forEach(item => {
        list.appendChild(createTaskTag(item));
    });
}


//------------------------------------------------------------------------
//new lesson

//event - объект с инфо о событии
//split('разделитель') -> превращает строку в массив по разделителю
//'самый! лучший! день'.split('!') --->  ['самый', 'лучший', 'день']


const button = document.querySelector('#sayHello');

/*
// способ через split
function itemClick(event) {
    const taskIdString = event.target.id; // 'task_1'
    const taskId = Number(taskIdString.split('_')[1]); // ['task', '1']
    const task = tasks.find(item => item.id === taskId);
    const index = tasks.indexOf(task);
    tasks.splice(index, 1);
    drawTask();
}
//
*/

// способ через filter
function itemClick(event) {
    const taskIdString = event.target.id; // 'task_1'
    const taskId = Number(taskIdString.split('_')[1]); // ['task', '1']
    tasks = tasks.filter(item => item.id !== taskId);
    drawTask();
    storageChange();
}
//

function addTask() {
    const input = document.querySelector('#taskName');
    const taskName = input.value;
    const newTask = {
        isComplete: false,
        name: taskName,
        id: getNewId() 
    };

    tasks.push(newTask);
    input.value = '';
    drawTask();
    storageChange();
    input.focus();

    //1. Забрать из localStorage содержимое (JSON string)
    //2. Распарсить строку (JSON.parse)
    //3. Добавить в распаршеный массив новую адачу
    //4. Прещбразовать полученный массив в JSoN стрщку (JSON.stringify)
    //5. Отправить этот массив в localStorage
    
}

function storageChange() {
    const newTaskJson = JSON.stringify(tasks);
    localStorage.setItem('tasks', newTaskJson);
}

/*
//через forEach, variable
function getNewId() {
    let max = 0;
    tasks.forEach(item => {
        if (item.id > max) {
            max = item.id;
        }
    });

    return max + 1;
}
//
*/

//через map, Math
function getNewId() {
    const ids = tasks.map(item => item.id); // [1, 2, 3, 4, 5]

    if (ids.length > 0) {
        const max = Math.max(...ids); // 5
        return max + 1;
    }

    return 0;
}
//

const addButton = document.querySelector('#add');
addButton.addEventListener('click', addTask);

const input = document.querySelector('#taskName');
input.addEventListener('keydown', checkEnterKey);

document.addEventListener('keydown', checkDeleteKey);


//button.addEventListener('click', itemClick); // params: название_событияб callback_func


// -------new lessn -----------

function getTasks() {
    const tasksStorage = localStorage.getItem('tasks');
    if (tasksStorage) {
        tasks = JSON.parse(tasksStorage);
    } else {
        tasks = [];
    }
}

function checkEnterKey(event) {
    if (event.keyCode === 13) {
        addTask();
    }
}

function checkDeleteKey(event) {
    if (event.keyCode === 46) {
        deleteLastTask();
    }
}

function deleteLastTask() {
        tasks.pop();
        storageChange();
        drawTask();
}

function toggleEdit(event) {
    const target = event.target;
    const classList = target.className;
    const isHaveActive = classList.includes('edit_active'); //new
    const taskName = target.parentNode.children[2];//new
    const editInput = target.parentNode.children[3];
    if (isHaveActive) {
        target.classList.remove('edit_active');
        taskName.classList.add('task__name_active');
        editInput.classList.remove('task__edit_active');
        const idString = target.id;
        const taskId = Number(idString.split('_')[1]);
        const currentTask = tasks.find(item => item.id === taskId);
        currentTask.name = editInput.value;
        drawTask();
        storageChange();
    } else {
        target.classList.add('edit_active');
        taskName.classList.remove('task__name_active');
        editInput.classList.add('task__edit_active');
    }
}